import { Component } from '@angular/core';

@Component({
  selector: 'landing-view',
  templateUrl: './landing.view.component.html'
})

export class LandingView { 

circleClick(event) {
	let elem = document.getElementById(event.target.id);
	if(elem){
		elem.classList.toggle("active");
	} else {
		let elems = document.getElementsByClassName('background');
		var count = elems.length;
		for(var i=0;i<count;i++){
			elems[i].classList.remove("active");
		}
	}
}

}